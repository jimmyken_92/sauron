class Frame(object):
    def __init__(self, data, grey, timestamp, video_id, image_id):
        self.data = data
        self.grey = grey
        self.timestamp = timestamp
        self.video_id = video_id
        self.image_id = image_id
        self.width = data.shape[1]
        self.height = data.shape[0]
