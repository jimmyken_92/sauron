class Detection(object):
	def __init__(self, label, roi_face, timestamp, frame_id, video_id):
		x1, x2, y1, y2 = roi_face
		self.label = label
		self.x1 = x1
		self.x2 = x2
		self.y1 = y1
		self.y2 = y2
		self.timestamp = timestamp
		self.frame_id = frame_id
		self.video_id = video_id
