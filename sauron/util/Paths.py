import os
cpath = os.path.dirname(os.path.abspath(__file__))

# get faces data
def facesData(): return os.path.join(cpath, 'encoding/dataset_encoding.pickle') # or json
