# controller
from sauron.modules.ComponentsController import ComponentsController

#import
import jsonpickle
from sauron.modules.Validations import age_validation

# import errors
from util.errors import Errors as error
# import types
from util.fileTypes import FileTypes as document

# import model
from ..models.Front import Front as BuildFrontResponse
from ..models.Back import Back as BuildBackResponse


ID_FRONT = document.ID_FRONT
ID_BACK = document.ID_BACK
SELFIE = document.SELFIE

# error
dataError422 = error.dataError422.value


class ResponseHandler(ComponentsController):
	def __init__(self, fileType):
		print('[ATTACHED] ResponseHandler')
		ComponentsController.__init__(self)

		self.fileType = fileType
		self.data = None
		self.awsResponse = None
		self.max_acceptable_range = 10 # check

	# dump the list of detections from memory to a .json file
	def dumpDetectionsToResponse(self):
		print('[RUNNING] ResponseHandler')
		responseData = ''

		# retrieve data and boxes
		if self.data is not None and self.boxes is not None:
			data = self.data[0]
			data_boxes = self.boxes[0]

		# response data
		#ci, name, surname, birthday, nationality, gender, marital_status = None, None, None, None, None, None, None

		# get key `y`
		#second_name, second_surname = False, False
		#birthplace = ''

		# print(self.fileType, fileType.ID_FRONT())

		# build data from front
		if self.fileType == ID_FRONT.value:
			fresult = ID_FRONT.value
			# build response
			responseData = BuildFrontResponse(ci=fresult.ci, name=fresult.name, surname=fresult.surname, birthday=fresult.birthday, birthplace=fresult.birthplace, gender=fresult.gender)
			# print(responseData)

		# build data from back
		if self.fileType == ID_BACK.value:
			fresult = ID_BACK.value
			# build response
			responseData = BuildBackResponse(nationality=bresult.nationality, marital_status=bresult.marital_status, expiration_date='')

		if self.data:
			return jsonpickle.encode(responseData)
		elif self.awsResponse:
			return jsonpickle.encode(self.awsResponse)
		else:
			return jsonpickle.encode(dataError422)

	# receiving data
	def get(self, data=None, awsResponse=None):
		if data is not None:
			self.data, self.boxes = data
		elif awsResponse is not None:
			self.awsResponse = awsResponse

		print('[INFO] ResponseHandler: Received new data, processing...')
