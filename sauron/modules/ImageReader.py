# controller
import threading
from sauron.modules.ComponentsController import ComponentsController

# import
import cv2
import numpy as np


class ImageReader(ComponentsController, threading.Thread):
	def __init__(self, received_image):
		print('[ATTACHED] ImageReader')
		ComponentsController.__init__(self)
		threading.Thread.__init__(self)

		self.image = received_image

	def run(self):
		print('[RUNNING] ImageReader')

		try:
			# create an array and read the image
			arr = np.frombuffer(self.image, dtype=np.uint8)
			img = cv2.imdecode(arr, cv2.IMREAD_COLOR)

			# save data 
			# with open('received_binary.json', 'a+') as file:
			# 	f.write(str(file))

			self.send(img)

		except Exception as e:
			print(f'[ERROR] {e}')

