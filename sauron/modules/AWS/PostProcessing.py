# controller
from sauron.modules.ComponentsController import ComponentsController

# imports
import yaml
from PIL import Image

# s3
import io
import boto3


with open('env.yaml') as data:
	env = yaml.load(data)

AWS_ACCESS_KEY_ID = env['AWS']['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = env['AWS']['AWS_SECRET_ACCESS_KEY']
REGION_NAME = env['AWS']['REGION_NAME']
AWS_S3_BUCKET_NAME = env['AWS']['AWS_S3_BUCKET_NAME']
# AWS_S3_URL = env['AWS']['AWS_S3_URL']


class PostProcessing(ComponentsController):
	def __init__(self, received_image, userSubject, fileType):
		print('[STARTED][AWS] PostProcessing')

		self.image = received_image
		self.userSubject = userSubject
		self.fileType = fileType

	def PostProcess(self):
		print('[RUNNING][AWS] PostProcessing')

		# open boto3 client
		client = boto3.client('s3',
			aws_access_key_id=AWS_ACCESS_KEY_ID,
			aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
			region_name=REGION_NAME
		)

		# upload results
		filename = f'{self.userSubject}/{self.fileType}'

		# image
		arr = Image.fromarray(image)
		file = io.BytesIO()
		arr.save(file, format='JPEG')
		file.seek(0)
		key = f'{filename}'

		# upload
		try:
			got_r = client.upload_fileobj(file, AWS_S3_BUCKET_NAME, key)
		except ClientError as e:
			print(e)

		print('[FINISHED][AWS] PostProcessing')
