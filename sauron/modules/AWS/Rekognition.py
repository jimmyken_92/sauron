# controller
from sauron.modules.ComponentsController import ComponentsController

# env
import os
from dotenv import load_dotenv
from PIL import Image

# s3
import io
import boto3


load_dotenv()


class RekognitionClass(ComponentsController):
	def __init__(self):
		print('[ATTACHED] Rekognition')
		ComponentsController.__init__(self)

		self.AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
		self.AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
		self.REGION_NAME = os.getenv('REGION_NAME')
		self.AWS_S3_URL = os.getenv('AWS_S3_URL')
		self.AWS_S3_BUCKET_NAME = os.getenv('AWS_S3_BUCKET_NAME')

	def Rekognition(self, images):
		print('[RUNNING] Rekognition')
		''' Upload a file to an S3 bucket

			:param file_name: File to upload
			:param bucket: Bucket to upload to
			:param object_name: S3 object name
		'''
		# upload gray images
		rekognition = boto3.client('rekognition',
			aws_access_key_id=self.AWS_ACCESS_KEY_ID,
			aws_secret_access_key=self.AWS_SECRET_ACCESS_KEY,
			region_name=self.REGION_NAME
		)

		# set rekognition_response dictionary
		rekognition_response = {
			'front': {},
			'rear': {}
		}
		imageface = 0
		for r_image in images:
			# print('Start OCR')

			image_array = Image.fromarray(r_image)
			stream = io.BytesIO()
			image_array.save(stream, format='JPEG')
			binary = stream.getvalue()

			if imageface == 0:
				rekognition_response['front'] = rekognition.detect_text(
					Image={ 'Bytes': binary }
				)
			else:
				rekognition_response['rear'] = rekognition.detect_text(
					Image={ 'Bytes': binary }
				)

			imageface += 1

		self.send(rekognition_response)

	def get(self, images):
		self.Rekognition(images)
