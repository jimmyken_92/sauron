# controller
from sauron.modules.ComponentsController import ComponentsController

# env
import os
from PIL import Image
import yaml

# imports
from util.signupTypes import SignupTypes as identificationDocumentUploadFields

# s3
import io
import boto3


with open('env.yaml') as data:
	env = yaml.load(data)

# required data
AWS_ACCESS_KEY_ID = env['AWS']['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = env['AWS']['AWS_SECRET_ACCESS_KEY']
REGION_NAME = env['AWS']['REGION_NAME']
AWS_S3_BUCKET_NAME = env['AWS']['AWS_S3_BUCKET_NAME']
AWS_S3_URL = env['AWS']['AWS_S3_URL']
BUCKET_NAME = env['AWS']['BUCKET_NAME']


class SelfieVerification(ComponentsController):
	def __init__(self, userSubject):
		print('[ATTACHED][AWS] Selfie Verification')
		ComponentsController.__init__(self)

		self.userSubject = userSubject

	def Rekognition(self, image):
		print('[RUNNING][AWS] Selfie Verification')
		''' Upload a file to an S3 bucket

			:param file_name: File to upload
			:param bucket: Bucket to upload to
			:param object_name: S3 object name
		'''
		# connection
		rekognition = boto3.client('rekognition',
			aws_access_key_id=AWS_ACCESS_KEY_ID,
			aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
			region_name=REGION_NAME
		)

		# set rekognition_response dictionary
		rekognition_response = {
			'data': {},
		}

		image_arr = Image.fromarray(image)
		stream = io.BytesIO()
		image_arr.save(stream, format='JPEG')
		binary_image = stream.getvalue()

		rekognition_response['data'] = rekognition.compare_faces(
			SourceImage={
				'S3Object': {
					'Bucket': BUCKET_NAME,
					'Name': binary_image
				}
			},
			TargetImage={
				'S3Object': {
					'Bucket': BUCKET_NAME,
					'Name': f'{identificationDocumentUploadFields.FRONT.value}/{self.userSubject}' # front image
				}
			},
		)

		# print(rekognition_response)
		self.send(awsResponse=rekognition_response)

	def get(self, image):
		self.Rekognition(image)
