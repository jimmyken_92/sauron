# imports
from datetime import datetime

# flag validations
minor = 'minor'
underage = 'underage'

def age_validation(received_date_str):
	flag = None

	# transform birthday into readable
	num = [int(i) for i in received_date_str]
	birthday = f'{num[4]}{num[5]}{num[6]}{num[7]}-{num[2]}{num[3]}-{num[0]}{num[1]}'
	now = datetime.now()

	# read data
	if birthday:
		# age verification
		birthday = datetime.strptime(birthday, "%Y-%m-%d")
		diff = now - birthday
		secs = diff.total_seconds()

		age = divmod(secs, 31536000)[0]

		# teenager (from 13-18)
		# forbidden age (child)
		if age > 12 and age < 18:
			flag = underage
		elif age < 13:
			flag = minor

	return flag
