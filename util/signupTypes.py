from enum import Enum


class SignupTypes(Enum):
	FRONT = 'document.front'
	REAR = 'document.back'
