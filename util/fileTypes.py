from enum import Enum


class FileTypes(Enum):
	ID_BACK = 'ID_BACK'
	ID_FRONT = 'ID_FRONT'
	SELFIE = 'SELFIE'
