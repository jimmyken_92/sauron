from enum import Enum


class Constants(Enum):
	PROCESSING_FILES_V1 = 'cmd.document-processing.process-file.v1'
	PROCESSING_RESULTS_V1 = 'fct.document-processing.ocr-results.v1'
