from enum import Enum


class Errors(Enum):
	imageError422 = { 'httpStatus': 422, 'errorCode': 'invalid.image.file', 'message': 'Invalid image extension' }
	dataError422 = { 'httpStatus': 422, 'errorCode': 'invalid.data', 'message': 'Invalid data' }
	error500 = { 'httpStatus': 500, 'errorCode': 'internal.server.error', 'message': 'Internal server error' }
