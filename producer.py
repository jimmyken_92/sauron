'''
Repo: https://github.com/confluentinc/confluent-kafka-python
Prerequisites: sudo apt install librdkafka-dev
Librdkafka repo: https://github.com/edenhill/librdkafka
Librdkafka author: Copyright (c) 2012-2020: Magnus Edenhill
Kafka Installation: pip install confluent-kafka
Kafka Installation (avro): pip install "confluent-kafka[avro]"
Author: Confluent
'''

# kafka consumer
from confluent_kafka import Producer

# imports
import yaml
from util.constants import Constants as topic


PROCESSING_RESULTS_V1 = topic.PROCESSING_RESULTS_V1

with open('env.yaml') as data:
    env = yaml.load(data)

broker_name = env['KAFKA']['BROKER']
topic = PROCESSING_RESULTS_V1.value
p = Producer({ 'bootstrap.servers': broker_name })


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print(f'Message delivery failed: {err}')
    else:
        print(f'Message delivered to {msg.topic()} [{msg.partition()}]')

def send(data, flowID, fileType, userSubject):
    # Create header to send
    headers = [(flowID), (fileType), (userSubject)]

    # Trigger any available delivery report callbacks from previous produce() calls
    p.poll(0)

    # Asynchronously produce a message, the delivery report callback
    # will be triggered from poll() above, or flush() below, when the message has
    # been successfully delivered or failed permanently.
    p.produce(topic, data.encode('utf-8'), headers=headers, callback=delivery_report)

# Wait for any outstanding messages to be delivered and delivery report
# callbacks to be triggered.
p.flush()
