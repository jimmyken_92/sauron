'''
Repo: https://github.com/confluentinc/confluent-kafka-python
Prerequisites: sudo apt install librdkafka-dev
Librdkafka repo: https://github.com/edenhill/librdkafka
Librdkafka author: Copyright (c) 2012-2020: Magnus Edenhill
Kafka Installation: pip install confluent-kafka
Kafka Installation (avro): pip install "confluent-kafka[avro]"
Author: Confluent
'''

# kafka consumer
from confluent_kafka import Consumer
from producer import send

# imports
import yaml
from util.constants import Constants as topic
from app import RunApp


PROCESSING_FILES_V1 = topic.PROCESSING_FILES_V1

with open('env.yaml') as data:
	env = yaml.load(data)

# required data
broker_name = env['KAFKA']['BROKER']
group_id = env['KAFKA']['GROUP']
topic_name = PROCESSING_FILES_V1.value

# create consumer
consumer = Consumer({
	'bootstrap.servers': broker_name,
	'group.id': group_id,
	'auto.offset.reset': 'earliest'
})

# subscribe to topic
consumer.subscribe([topic_name])


while True:
	try:
		msg = consumer.poll(1.0)

		if msg is None:
			continue
		if msg.error():
			print(f'Consumer error: {msg.error()}')
			continue

		# get image file
		received_flowID, received_fileType, received_userSubject = msg.headers()
		received_image = msg.value()

		fileType = received_fileType[1].decode('ascii')
		userSubject = received_userSubject[1].decode('ascii')

		response = RunApp(fileType, received_image, userSubject)
		send(response, received_flowID, received_fileType, received_userSubject)

		# post processing
		postProcessing = PostProcessing(received_image, userSubject, fileType)
		# upload images
		postProcessing.PostProcess()

	except:
		# print(e)
		traceback.print_exc()

	'''
	If an exception occurs, this line would end the process
	and it needs to restart it again
	'''
	# finally:
	# 	consumer.Close()

consumer.Close()
