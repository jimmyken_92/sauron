# import modules
from sauron.modules.ImageReader import ImageReader
from sauron.modules.DocumentOCR import DocumentOCR
from sauron.modules.ResponseHandler import ResponseHandler
from sauron.modules.AWS.PostProcessing import PostProcessing
from sauron.modules.AWS.SelfieVerification import SelfieVerification

# imports
from util.fileTypes import FileTypes as document


SELFIE = document.SELFIE


def RunApp(fileType, received_image, userSubject):
	# set components
	components = []

	if fileType != SELFIE.value:
		component = DocumentOCR()
	elif fileType == SELFIE.value:
		component = SelfieVerification(userSubject)

	imageReader = ImageReader(received_image)
	responseHandler = ResponseHandler(fileType)

	# add components
	components.append(imageReader)

	# subscribe to modules -> expected results
	imageReader.subscribe(component) # -> ocr detections
	component.subscribe(responseHandler) # -> result

	# start running components
	print('Started')
	for component in components:
		component.start()

	for component in components:
		component.join()
	print('Finished')

	return responseHandler.dumpDetectionsToResponse()
